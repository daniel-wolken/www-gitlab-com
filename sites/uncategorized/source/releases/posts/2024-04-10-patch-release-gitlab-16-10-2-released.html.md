---
title: "GitLab Patch Release: 16.10.2, 16.9.4, 16.8.6"
categories: releases
author: Greg Alfaro
author_gitlab: truegreg
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 16.10.2, 16.9.4, 16.8.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/04/10/patch-release-gitlab-16-10-2-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.10.2, 16.9.4, 16.8.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Stored XSS injected in diff viewer](#stored-xss-injected-in-diff-viewer) | High |
| [Stored XSS via autocomplete results](#stored-xss-via-autocomplete-results) | High |
| [Redos on Integrations Chat Messages](#redos-on-integrations-chat-messages) | Medium |
| [Redos During Parse Junit Test Report](#redos-during-parse-junit-test-report) | Medium |

### Stored XSS injected in diff viewer

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.9 before 16.9.4, all versions starting from 16.10 before 16.10.2. A payload may lead to a stored XSS while using the diff viewer, allowing attackers to perform arbitrary actions on behalf of victims.
This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`, 8.7).
It is now mitigated in the latest release and is assigned [CVE-2024-3092](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3092).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Stored XSS via autocomplete results

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.7 to 16.8.6 all versions starting from 16.9 before 16.9.4, all versions starting from 16.10 before 16.10.2. Using the autocomplete for issues references feature a crafted payload may lead to a stored XSS, allowing attackers to perform arbitrary actions on behalf of victims.
This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`, 8.7).
It is now mitigated in the latest release and is assigned [CVE-2024-2279](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2279).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Redos on Integrations Chat Messages

A denial of service vulnerability was identified in GitLab CE/EE, versions 16.7.7 prior to 16.8.6, 16.9 prior to 16.9.4 and 16.10 prior to 16.10.2 which allows an attacker to spike the GitLab instance resources usage resulting in service degradation via chat integration feature.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2023-6489](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6489).

Thanks `Anonymizer` for reporting this vulnerability through our HackerOne bug bounty program.


### Redos During Parse Junit Test Report

An issue has been discovered in GitLab EE affecting all versions before 16.8.6, all versions starting from 16.9 before 16.9.4, all versions starting from 16.10 before 16.10.2. It was possible for an attacker to cause a  denial of service using malicious crafted content in a junit test report file.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2023-6678](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6678).

Thanks `Anonymizer` for reporting this vulnerability through our HackerOne bug bounty program.



## Bug fixes


### 16.10.2

* [Quarantine flaky atomic processing ResetSkippedJobsService specs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147975)
* [Fix include_optional_metrics_in_service_ping during migration to 16.10](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148103)
* [Use alpine:latest instead of alpine:edge in CI images [16.10]](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148227)
* [[16.10] Backport Delete callback should use namespace_id](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147937)
* [[16.10] Backport handle null owner when indexing projects](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148417)
* [Backport Zoekt: Retry indexing if too many requests to 16.10](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147942)
* [Backport https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148596](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148687)
* [Fix URL validator for mirror services when using localhost](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148663)
* [Backport !148105 into 16.10](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148571)
* [Cherry-pick 'fix-omnibus-gitconfig-deprecation' into '16-10-stable'](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7503)

### 16.9.4

* [Quarantine flaky atomic processing ResetSkippedJobsService specs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147985)
* [Use alpine:latest instead of alpine:edge in CI images [16.9]](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148229)

### 16.8.6

* [Quarantine flaky atomic processing ResetSkippedJobsService specs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147986)
* [Use alpine:latest instead of alpine:edge in CI images [16.8]](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148231)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
