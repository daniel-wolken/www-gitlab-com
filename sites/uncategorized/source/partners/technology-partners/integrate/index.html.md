---
layout: handbook-page-toc
title: "Alliance Partner Onboarding"
description: GitLab is open to collaboration and committed to building Alliance partnerships and solutions for the DevOps community. Through product integrations, GitLab helps developers compile all their work into one tool that can be accessed anywhere. We work closely through partnerships to provide developers with a single DevOps experience.
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


#### Welcome to the GitLab Alliance Partner Program Guide.

Here at GitLab we value the contributions from our community and believe our [partners](https://about.gitlab.com/partners/technology-partners) play an instrumental role in building powerful technology solutions. Our Alliance Partner program is focused on the development of technology integrations that can be used by those around the world who trust GitLab as their DevSecOps platform.

Our Alliance partner program offers you with helpful resources to get started on your journey as you explore ways to build your integration and share with the GitLab community. As partners, we have an opportunity to collaborate together to deliver the best curated cloud-native solutions so thank you for partnering with us!

## Onboarding Process


### 📌 Step 1: Partner Registration
Register to become a GitLab Alliance Partner.

* Submit a new Partner Application on the [Partner Portal](https://partners.gitlab.com/)
    * Select **_Alliance Partner Application_** as the Partner Company Category
* Complete the form with all required information
    * Select **_Cloud Partner, Platform Partner, or Technology (ISV) Partner_** as the Partner Type
* Review the Technology Partner Agreement
    * **_Submit_** the application


### 📌 Step 2: Solution Development
 We ask that each of our partners contribute a technology integration!  Be sure to visit the [GitLab Developer Portal](https://developer.gitlab.com/) for helpful resources like APIs, webhooks and more, that will help you write code in the GitLab codebase and design experiences in the GitLab UI. For [Secure](https://about.gitlab.com/direction/secure/) and/or Govern stage Integrations, please check out the [Secure Partner Integration - Onboarding Process](https://docs.gitlab.com/ee/development/integrations/secure_partner_integration.html) for more information.

A great way to showcase your solution is by building a component in the [GitLab CI/CD Catalog](https://docs.gitlab.com/ee/ci/components/index.html#component-project). There’s no cap on components and the approval process is entirely automated, so no need for manual approval from GitLab, which streamlines the contribution workflow. Also, Partner’s who register their component in the partner portal will receive a badge signifying that the component is created by a trusted GitLab partner and will be prioritized within the catalog. This will help promote your component with countless developers who love GitLab!


**What if I need help with building my integration?** 
* Log into the Technology Partner Portal to request a Not-for-Sale license for validating/testing the integration.
* Check out the wonderful [GitLab Community](https://about.gitlab.com/community/) where you can find helpful documentation and collaborate through the [GitLab Community Forum](https://forum.gitlab.com/). 

**How do I register my GitLab CI/CD component to receive the partner badge?**
* Log into the Technology Partner Portal and update your partner account profile to include the namespace URL that is associated with your component.

**Who is responsible for maintaining the CI/CD Catalog component or other integrations?**
* Partners take on the role of maintainers for integration or the components they publish in the CI/CD Catalog. This responsibility includes regular upgrades, maintenance, and fixing of any issues that may arise.

**What else can I do while I am building my integration?** 
* Learn how to develop components using our [official development guide](https://docs.gitlab.com/ee/development/cicd/components.html)
* Learn more about [GitLab’s product direction](https://about.gitlab.com/direction/)
* Become a GitLab [Community Contributor](https://about.gitlab.com/community/contribute/)
* Log into the Partner Portal to learn more about GitLab and other opportunities where we can partner together.


### 📌 Step 3: Solution Documentation and Demonstration

1.  Create technical documentation on the integrated solution and make it publicly available on your website.
2.  Create solution value messaging (solution benefits, and business and/or technical use cases)


### 📌 Step 4: Partnership and Solution Promotion

Once all the hard work of building and documenting the integration is completed, let’s get to work promoting the solution! Below are a few areas where we can partner together. 


* Add the [GitLab logo](https://about.gitlab.com/press/press-kit/#logos) on your company's website. Please make sure to follow [GitLab's branding guidelines](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/brand/brand-activation/brand-standards/).
* **Partner Listing:** Partners who have created a CI/CD Catalog component will be eligible for being listed on the [GitLab Partner page](https://about.gitlab.com/partners/technology-partners/).
* Additional opportunities that include Solution Briefs, blogs, webinars and more will be made available to partners who go above and beyond in demonstrating alignment with GitLab's product roadmap and where there’s growing customer demand.

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Additional Resources & Support

#### Not-for-Sale License Program and Policy
GitLab offers qualified GitLab Technology Partners access to our solutions at no cost through a Not-for-Resale (NFR) license for GitLab SaaS and GitLab Self-Managed versions. This provides an opportunity to develop and test your integration on the GitLab platform. For more information on how to request or renew an existing NFR license, log into the Technology Partner Portal.

* To be eligible for consideration to receive a NFR license, partners must be registered as a GitLab Technology Partner.
* Partners can choose licenses to support integrations with GitLab SaaS and/or GitLab Self-Managed.
* Licenses will be issued for 12 months, and for up to 10 users upon request.
* GitLab provides no support for NFR software issued to Technology Partners.


**NFR Program Terms and Conditions:**

1. NFR software and services may be used solely and exclusively by the partner for the following purposes:
    - Internal employee training
    - Integration testing with related devops products and platforms
    - Partner led product demonstrations to prospective customers
2. Partner in-house production use for customer engagements or internal development efforts requires purchased GitLab licenses which are available to partners at a discount. Use of the NFR licenses in a customer environment, including for managed services is strictly prohibited.
3. Alliance Partners may request a license for up to 10 users without additional approval.
4. All software obtained under the NFR Program are subject to the terms and conditions of the GitLab Subscription Agreement at https://about.gitlab.com/terms/
5. GitLab reserves the right to audit the use of NFR licenses to ensure they are in compliance with the NFR program, and reduce the number of licenses to a partner if they are not in compliance with the program.
6. GitLab reserves the right to reject a partner request for an NFR or otherwise change or cancel the NFR Program at any time and for any or no reason.


#### Contact Us

Don’t forget, we are here to help. If you have any questions or feedback, you can reach us at technologypartners@gitlab.com. If it’s technical assistance you’re looking for, please see below for troubleshooting.


#### Community Engagement

We also encourage our partners to participate in the GitLab community, for example: [contributing](https://about.gitlab.com/community/contribute/) to GitLab FOSS, hosting a [GitLab Virtual Meetup](https://about.gitlab.com/community/virtual-meetups/), participating in [GitLab Heroes](https://about.gitlab.com/community/heroes/), or engaging the community in other ways. Partners are welcome to bring questions or ideas around growing our communities directly to our Developer Relations team via [evangelists@gitlab.com](mailto:evangelists@gitlab.com).


## Are you an individual contributor to GitLab wanting to share your work with the open source community?
Join our community of 3,000+ contributors. To get started, visit our [GitLab Community page](https://about.gitlab.com/community/) and learn more about resources, programs, and events.

