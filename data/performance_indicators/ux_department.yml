- name: Total open UX bug issues by severity
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: The purpose of this chart is to show the total volume of existing UX bug issues that impact our SUS score. We are tracking against the label "bug::ux."
  target: 0 no severity issues, and 0 S1/S2 issues behind the SLA due date
  sisense_data:
    chart: 13419472
    dashboard: 934130
    embed: v2
  org: UX Department
  is_key: true
  health:
    level: 2
    reasons:
    - We are refining this KPI in FY25-Q1 to target UX bugs by severity and their open/close rates. We will be able to reduce the noise created in issues by the bot automation that required manual intervening, provide focus to the resolution of UX bugs, and monitor the volume of UX bugs continuing to enter the product.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
- name: Technical Writer MR Rate
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: This PI tracks the number of MRs merged every month using the Technical Writing and UI text labels across all GitLab projects where the team works. The December rate was impacted by team PTO. Team performance has understandably changed throughout the years of this PI, based on changes to team organization and role requirements. We are revisiting this PI.
  target: 55 MRs per technical writer per month
  org: UX Department
  is_key: true
  health:
    level: 2
    reasons:
    - This target rate remains somewhat aspirational.
  dri: <a href="https://gitlab.com/susantacker">Susan Tacker</a>
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/t/gitlab/views/DRAFT-UXKPIs/TechnicalWritingteammemberMRRate
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: Average research projects per Product Designer
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Our goal is to use customer research to validate problems and solutions to ensure we
    are building the right things in the right way. We use many research methods,
    including interviews, surveys, usability studies, findability/navigation studies,
    and analytics. Hypothesis that there is a connection between this KPI and SUS
    KPI.
  target: At or greater than 2 validation issues per Product Designer per quarter
  sisense_data:
    chart: 7004937
    dashboard: 462325
    embed: v2
  org: UX Department
  is_key: true
  health:
    level: 2
    reasons:
    - Below target for most of FY24.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
- name: Product Design MR review volume
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Our goal is to provide UX reviews for Merge Requests (MRs) that involve user-facing changes, including those impacting screen readers, to help improve the quality of our product and reduce the amount of Deferred UX. Product Designers follow our <a href="/handbook/product/ux/product-designer/mr-reviews/">MR Review guidelines</a> to conduct these reviews.
  target: At or greater than 7 MR Reviews per Product Designer per month.
  org: UX Department
  is_key: true
  health:
    level: 0
    reasons:
    - FY25-Q2 is the first full quarter we will track this indicator per Product Designer. Target is based on previous historical data by total volume per momth.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/UXDepartmentMRRate/ProductDesignReviewRequest?:iid=2
        height: 400px
        toolbar: hidden
        hide_tabs: true
- name: UX Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: UX Department
  is_key: true
  public: false
  health:
    level: 2
    reasons:
    - Below target with indicators that this trend will continue in the short term.
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/views/N5AttritionDashboard/AttritionDashboard/e1946f41-00bf-43e5-aca2-a46cf95174ac/UXAttrition-Dashboard?:iid=1"
- name: UX Average Age of Open Positions
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-vacancy-time-to-fill"
  definition: Measures the average time job openings take from open to close. This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: UX Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Consistently higher than the target. Making adjustments in our approach in FY25.
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/WIP-HirePlanningDashboardSimplified/HirePlanningDashboard?:iid=1
        height: 400px
        toolbar: hidden
        hide_tabs: true
- name: System Usability Scale (SUS) score
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: The <a href="/handbook/product/ux/performance-indicators/system-usability-scale/">System Usability Scale (SUS)</a> is an industry-standard survey that
    measures overall system usability based on 10 questions. Moving a SUS score upward
    even a couple of points on a large system is a significant change. The goal of
    this KPI is to understand how usability of the GitLab product rates against industry
    standards and then track trends over time. Even though UX will be responsible
    for this metric, they will need other departments such as PM and Development to
    positively affect change. See <a href="/handbook/product/ux/performance-indicators/system-usability-scale/index.html">our grading scale</a> for details on interpreting scores. SUS data is collected every other quarter.

  target: 73 by Q4-FY24, 77 by Q4-FY25, 82 by Q4-FY26
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/t/gitlab/views/DRAFT-UXKPIs/SystemUsabilityScoreMakeGitLabthemostusableDevOpstoolonthemarket
        height: 400px
        toolbar: hidden
        hide_tabs: true

  org: UX Department
  is_key: false
  health:
    level: 1
    reasons:
    - Perceived usability rates as a C+. Overall, we have seen a declining trend in SUS with periods of stabilization.
    - FY22-Q1 focused on <a href="https://gitlab.com/gitlab-com/Product/-/issues/2099">performance and visibility of system status</a>.
    - FY22-Q2 focused on <a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11204">Merge Request improvements</a>.
    - FY22-Q3 included 104 Pajamas migrations.
    - FY22-Q4 included 209 Pajamas migrations.
    - FY23-Q1 included 331 Pajamas migrations and burn down of <a href="https://gitlab.com/gitlab-org/gitlab-design/-/issues/1854">9 S1 SUS-Impacting issues</a>.
- name: Customer Satisfaction (CSAT) score
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: The Customer Satisfaction (CSAT) is an industry-standard survey that
    measures user satisfaction based on a single question (How satisfied are you with GitLab (the product)?). The goal of
    this KPI is to understand how customer satisfaction of the GitLab product rates against industry
    standards and then track trends over time. Even though UX will be responsible
    for this metric, they will need other departments such as PM and Development to
    positively affect change. CSAT data is collected every other quarter. More information about this KPI is coming soon in early FY25 Q2.

  target: TBD
  tableau_data:
    charts:
      - url: 
        height: 
        toolbar: 
        hide_tabs: 
  org: UX Department
  is_key: 
  health:
    level: 
    reasons:
- name: Experience baselines
  base_path: "/handbook/product/ux/performance-indicators/"
  definition:  This PI measures the number of total categories with and without baseline experience scores from formative evaluation UX Scorecards of a main JTBD.
  target: 100% of supported categories have determined their experience baseline.
  org: UX Department
  is_key: false
  health:
    level: 3
    reasons:
    - In FY25Q1, we have a KR to create main JTBD for each supported category. Additionally, we have a KR to perform a formative scorecard for three categories.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
- name: Experience baseline scores
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: This PI tracks the experience baseline scores of each category.
  target: 100% of supported categories have an experience baseline score of B or higher.
  org: UX Department
  is_key: false
  health:
    level: 4
    reasons:
    - We first need to generate scores for all categories in order to understand which categories we need to prioritize to meet the goal of a B or greater.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
- name: UX bug issues opened/closed each month
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: With SUS as a KPI, it's important to ensure that we are closing UX bugs issues at an appropriate velocity. UX is responsible for ensuring that issues are opened when appropriate and advocating for their prioritization, while Product Management is the ultimate DRI for prioritization. We are tracking against the label "bug::ux."
  target: TBD
  org: UX Department
  is_key: false
  health:
    level: 0
    reasons:
    - We will be revising this PI in FY25 to give focus to a smaller number of labels.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/DRAFT-UXKPIs/SUSopenclosedovertime
        height: 400px
        toolbar: hidden
        hide_tabs: true
- name: Pajamas component migrations
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Integrating Pajamas components into GitLab contributes to a cohesive and
    consistent user experience, visually and functionally. This allows users to seemlessly
    transition throughout different stages of the DevOps lifecycle. With our adoption scanner, we are able
    to track percent adoption of existing Pajamas components per stage group. This PI does not yet track usage of
    components within the product that exist outside of Pajamas. 
  target: 100% of groups are "On track"
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
     - 64% (27/43) of groups are "On track"
     - 29% (12/43) of groups "Need attention"
     - 0% (0/43) of groups are "At risk"
     - 9% (4/43) of groups don't have enough data to measure
  dri: <a href="https://gitlab.com/tauriedavis">Taurie Davis</a>
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/workbooks/2367749/views"
- name: Usability benchmarking overall score by stage
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: This PI tracks the overall stage score for <a href="https://about.gitlab.com/handbook/product/ux/ux-research/usability-benchmarking/">usability benchmarking</a> studies performed across stage groups as they change over time. The tasks and workflows that comprise each benchmarking study are derived from JTBD for one or more target personas typical for the stage running the study. The overall score for each study takes into account the performance of each task that was tested, through metrics like completion rate, severity, and customer effort score (CES). The scale is 0-100, where 90-100 is ‘Great’, 80-89 is ‘Good’, 70-79 is ‘Fair’, 69 and below is ‘Poor’.
  target: 5% increase in overall score from previous benchmarking, maintaining an overall score above 84/100.
  org: UX Department
  is_key: false
  health:
    level: 0
    reasons:
    - Not enough data. Only one benchmarking study has been performed so far.
  dri: <a href="https://gitlab.com/asmolinski2">Adam Smolinski</a>
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/PDUXUsabilityBenchmarkingResults/Table?:iid=2
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: UX Department MR Rate
  base_path: "/handbook/product/ux/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: UX Department MR Rate is a performance indicator showing how many changes the UX team implements
    directly in the GitLab product. We currently count all members of the UX Department
    (Directors, Managers, ICs) in the denominator, because this is a team effort.
    The full definition of MR Rate is linked in the url section.
  target: Greater than TBD MRs per month
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    - We don't yet know what a good MR rate looks like for UX. Need accurate data
      to determine.
    - UX MR rate doesn't accurately reflect all MRs to which UX contributes, because
      we often collaborate on MRs rather than opening them ouselves.
  urls:
    - "/handbook/engineering/metrics/#merge-request-rate"
    - https://gitlab.com/gitlab-data/analytics/-/issues/4448
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/UXDepartmentMRRate/UXDept_MRRate?:iid=1
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: UX Department Discretionary Bonus Rate
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
      - Metric is new and is being monitored
  tableau_data:
     charts:
       - url: https://10az.online.tableau.com/#/site/gitlab/views/DiscretionaryBonusDashboard/DiscretionaryBonusDashboard/aafdd426-a965-4921-be0e-7d74fc907e4c/UXQuarter-to-DateDiscretionaryBonusRate?:iid=1
         height: 400px
         toolbar: hidden
         hide_tabs: true
- name: Actionable insights
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Actionable insights originate from user research. They always have the 'Actionable Insight' label applied to the resulting issue and a clear follow up that needs to take place as a result of the research observation or data. An actionable insight both defines the insight and clearly calls out the next step as a recommendation. The goal of this KPI is to ensure we're documenting research insights that are actionable and tracking their closure rate.
  target: TBD
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/t/gitlab/views/DRAFT-UXKPIs/IssueswithActionableInsightlabels/943fb7df-7ca0-4b1e-92b3-0a750f0873f3/25a3e302-0d9e-4932-84ea-0e7dae36bc39
        height: 400px
        toolbar: hidden
        hide_tabs: true

  org: UX Department
  is_key: false
  health:
    level: 3
    reasons:
    - Q3 FY21 was spent establishing a baseline.  Now that there's ample data available, we'll take two steps.  Step 1 - investigate the oldest open actionable insights to understand why they have not been closed.  Step 2 - track the average time for actionable insights to be closed.
  dri: <a href="https://gitlab.com/asmolinski2">Adam Smolinski</a>
- name: Deferred UX
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: <a href="https://about.gitlab.com/handbook/engineering/workflow/#deferred-ux">Deferred UX</a> means that for a given issue, we failed to meet defined standards
    for our Design system or for usability and feature viability standards as defined
    in agreed-upon design assets. When we fail to ship something according to defined
    standards, we track the resulting issues with a <a href="https://handbook.gitlab.com/handbook/engineering/workflow/#deferred-ux">"Deferred UX"</a> label. Even though UX
    will be responsible for this metric, they will need other departments such as
    PM and Development to positively affect change.
  target: Below 50 open "deferred UX" issues
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/t/gitlab/views/DRAFT-UXKPIs/OpenUXDebtissuesovertime
        toolbar: hidden
        hide_tabs: true

  org: UX Department
  is_key: false
  health:
    level: 1
    reasons:
    - Total amount of Deferred UX has increased in the last several months and is well above the target.
    - We are actively working with PMs to prioritize Deferred UX. Some stage groups are
      committing to resolving a minimum number of Deferred UX issues per milestone (generally,
      a commitment of no less than one issue). We will track this effort and make
      adjustments as we see the results.
    - The Deferred UX label has been inconsistently applied likely due to it deviating from the industry standard term. We are exploring alternatives to track Deferred UX in FY25 to increase accuracy.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>
- name: Open Deferred UX Age
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Age of outstanding <a href="https://about.gitlab.com/handbook/engineering/workflow/#deferred-ux">Deferred UX</a> issues. Deferred UX means that for a given issue, we failed to meet defined standards. Age represented via median of days opened.

  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/t/gitlab/views/DRAFT-UXKPIs/OpenUXDebtAge
        height: 400px
        toolbar: hidden
        hide_tabs: true

  org: UX Department
  target: At or below 150 days
  is_key: false
  health:
    level: 2
    reasons:
      - Average days to close a "Deferred UX" issue is beginning to trend upward.
      - We will monitor to see if the trend continues.
  dri: <a href="https://gitlab.com/vkarnes">Valerie Karnes</a>

- name: Technical Writing collaboration on UI text
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Historically, Technical Writers were not consistently included in the creation of UI text. Since UI text is critical to product usability, Technical Writing involvement can help improve the quality of our UI.
    This chart includes issues and MRs with the Technical Writing and UI text labels, because Technical Writing contributions happen in both places.
  target: TBD
  org: UX Department
  is_key: false
  health:
    level: 0
    reasons:
    - We are watching this metric to determine a target, because historical data is inconsistent.
  dri: <a href="https://gitlab.com/susantacker">Susan Tacker</a>
  sisense_data:
    chart: 12963522
    dashboard: 462325
    embed: v2
- name: Product Designer Gearing Ratio
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Number of Product designers against the targeted <a href="https://about.gitlab.com/handbook/product/ux/how-we-work/#headcount-planning">gearing ratio</a>
  target: At 57 product designers
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    -  At 47% of targeted gearing ratio
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/UXGearingRatios/ProductDesigner
        height: 400px
        toolbar: hidden
        hide_tabs: true
- name: Technical Writer Gearing Ratio
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Number of Technical Writers against the targeted <a href="https://about.gitlab.com/handbook/product/ux/how-we-work/#headcount-planning">gearing ratio</a>
  target: At 19 technical writers
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    -  At 63% of targeted gearing ratio
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/UXGearingRatios/TechnicalWriter?:iid=1
        height: 400px
        toolbar: hidden
        hide_tabs: true
- name: UX Researcher Gearing Ratio
  base_path: "/handbook/product/ux/performance-indicators/"
  definition: Number of researchers against the targeted <a href="https://about.gitlab.com/handbook/product/ux/how-we-work/#headcount-planning">gearing ratio</a>
  target: At 11 researchers
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    -  At 72% of targeted gearing ratio
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/UXGearingRatios/UXResearcher?:iid=1
        height: 400px
        toolbar: hidden
        hide_tabs: true
- name: UX Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: UX Department
  is_key: false
  health:
    level: 3
    reasons:
      - Metric is new and is being monitored
  tableau_data:
    charts:
      - url: https://10az.online.tableau.com/#/site/gitlab/views/HeadcountDashboard/HeadcountDashboard/dc2cab89-b092-4ae4-bdf4-1d05f1f69b33/UXHeadcount-Dashboard?:iid=2
        height: 400px
        toolbar: hidden
        hide_tabs: true
